from django.contrib import admin
from polls.models import Question
from django import forms
import ast
# Register your models here.

CHOICES = (
	('first', 'FIRST'),
	('second', 'SECOND'),
	('third', 'THIRD'),
)

class QuestionAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(QuestionAdminForm, self).__init__(*args, **kwargs)
        # initial_values = (
        # 	('first', 'First'), 
        # 	('second', 'Second')
        # )
        # import pdb; pdb.set_trace()
        self.fields['question_text'] = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CHOICES)
        # self.fields['question_text'].initial = ast.literal_eval(self.instance.question_text)

class QuestionAdmin(admin.ModelAdmin):
    form = QuestionAdminForm

admin.site.register(Question, QuestionAdmin)